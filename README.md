# NFT Marketplace testassignment

Truffle project is based on box "nft-marketplace".

## Client 

Next.js with rainbowkit.
Connection to web3 is done using @walletconnect/web3-provider
web3modal failed to work under Opera

## Notes 

Contract tested with locally started Ganache
Attempted to connect to infura, not verified due to large lattency.