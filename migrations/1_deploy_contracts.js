var FluffyHedgehogNFT = artifacts.require("FluffyHedgehogNFT");
var Marketplace = artifacts.require("Marketplace");

module.exports = async function(deployer) {
  await deployer.deploy(Marketplace);
  const marketplace = await Marketplace.deployed();
  await deployer.deploy(FluffyHedgehogNFT, marketplace.address);
}
