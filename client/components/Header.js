import {
    useConnectModal,
    useAccountModal,
    useChainModal,
} from '@rainbow-me/rainbowkit';
import { useAccount } from 'wagmi'
import { FaHome } from "react-icons/fa";
import Link from 'next/link';

export default function Header() {
    const { openConnectModal } = useConnectModal();
    const { openAccountModal } = useAccountModal();
    const { openChainModal } = useChainModal();

    const { address, isConnected } = useAccount();

    return (
        <div className="navbar text-neutral-content bg-primary-content">
            <div className="flex-1 ml-3 text-gray-50">
                <ul className='flex flex-row justify-between gap-6'>
                    <li><Link href="/"><FaHome size="2rem" /></Link></li>
                    {isConnected && <li><Link href="/shop">SHOP</Link></li> }
                    {isConnected && <li><Link href="/assets">ASSETS</Link></li> }
                </ul>
            </div>

            <div className="navbar-end">
                {isConnected ?
                    (<><button className="btn btn-sm btn-info ml-3 normal-case" onClick={openAccountModal}>Profile</button><button className="btn btn-sm btn-error ml-3 normal-case " onClick={openChainModal}>Chain</button></>)
                    :
                    (<button className="btn btn-sm btn-error ml-3 normal-case" onClick={openConnectModal}>connect wallet</button>)
                }
            </div>
        </div >
    )
}