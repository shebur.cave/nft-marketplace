import React from "react";

import {
  useContractRead,
  useContractWrite
} from "wagmi";

import Marketplace from '../../contracts/ethereum-contracts/Marketplace.json'

import { FaDollarSign } from "react-icons/fa";

const contractAddress = '0xd33D1fA9dcf4B6121E9e6Dc05eBd3b57fc104cEe'; // local ganache

export default function Shop() {

  const {refetch, data} = useContractRead(
    {
      address: contractAddress, 
      abi: Marketplace.abi,
      functionName: 'getListedNfts',
    }
  );

  const { isSuccess, write } = useContractWrite({
    address: contractAddress, 
    abi: Marketplace.abi,
    functionName: 'buyNfts'
  })

  const buyNFT = (nft) => {
    console.log("buy NFT")
    //write(nft)
    //refetch();
  }

  return (
    <div className="hero min-h-screen">
      <div className="hero-content text-center flex flex-col">
        <div className="max-w-md">
          { !!data 
              ? data.map(nft =>
                <div className="card card-compact hover:bg-base-400 transition-all duration-400 hover:-translate-y-1">
                  <figure className="px-4 pt-4"><FaDollarSign size="6rem" /></figure> 
                  <div className="card-body">
                    <h2 className="text-center text-lg font-bold">nft.name</h2>
                  </div>
                  <button type="button" class="button" onClick={() => buyNFT(nft)}>BUY</button>
                </div>
              ) : (
                <div className="card-body"><h2 className="text-center text-lg font-bold">NO NFTs LISTED</h2></div>
              )
            }
        </div>
      </div>
    </div>
  )
}
