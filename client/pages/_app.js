import '../styles/globals.css'
//wagmi.
import { mainnet, sepolia } from 'wagmi/chains'
import { WagmiConfig, createClient, configureChains } from 'wagmi'
import { publicProvider } from 'wagmi/providers/public';
import { infuraProvider } from 'wagmi/providers/infura'
//rainbow kit UI framework.
import '@rainbow-me/rainbowkit/styles.css';
import '../styles/Home.module.css';
import Layout from '../components/layout'

import {
  getDefaultWallets,
  RainbowKitProvider,
} from '@rainbow-me/rainbowkit';

const { chains, provider } = configureChains(
  [mainnet, sepolia], 
  [publicProvider(), infuraProvider({ apiKey: '91e35a8436204c0b8069bc31cf77c4e5' })]
)

const { connectors } = getDefaultWallets({
  appName: 'Fluffy Hedgehog NFT Marketplace',
  chains
});

const client = createClient({
  autoConnect: true,
  connectors,
  provider,
})

function MyApp({ Component, pageProps }) {
  return (
    <WagmiConfig client={client}>
      <RainbowKitProvider chains={chains}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </RainbowKitProvider>
    </WagmiConfig>
  )

}

export default MyApp
