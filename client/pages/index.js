import React from "react";

export default function Home() {
  return (
    <div className="hero min-h-screen">
        <div className="hero-content text-center flex flex-col">
          <div className="max-w-md">
            <h1 className="text-5xl font-bold">Fluffy Hedgehog NFT Marketplace</h1>
            <p className="py-6 text-xl font-normal leading-normal mt-0 mb-2">The fluffy hedgehog is an enchanting contradiction, blending prickly defenses with a soft, velvety exterior. Its quills, a shield against the world, conceal a heartwarming charm. With eyes that gleam like obsidian gems, it captures hearts as it waddles through forests and gardens. Nature's unique creation, the fluffy hedgehog reminds us that even amidst life's thorns, there's an undeniable sweetness that deserves our admiration and protection.</p>
          </div>
        </div>
      </div>
  )
}
